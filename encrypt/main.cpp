#include <iostream>
#include <string>
#include <map>

using namespace std;

class Rotor_letters {
public:
	string original	      = "ABCDEFGHIJKLMNOPQRSTUVWXYZ+";
	string rotor1_encrypt = "DMTWSILRUYQNKFEJCAZBPGXOHV-";
	string rotor2_encrypt = "HQZGPJTMOBLNCIFDYAWVEUSRKX+";
	string rotor3_encrypt = "UQNTLSZFMREHDPXKIBVYGJCWOA-";
	map<char, char> rotor1_maps, rotor2_maps, rotor3_maps;
	int r1_possition = 0, r2_possition = 0, r3_possition = 0;

public:

	map<char, char>Encrypted_letters(map<char, char>& rotorx_maps, string original, int rotor)
	{
		switch(rotor)
		{
		case 1: 
			for (int i = 0; i < 28; i++) 
				rotorx_maps.insert(pair<char, char>(original[i], rotor1_encrypt[i]));
			break;
		case 2:
			for (int i = 0; i < 28; i++)
				rotorx_maps.insert(pair<char, char>(rotor1_encrypt[i], rotor2_encrypt[i]));
			break;
		case 3:
			for (int i = 0; i < 28; i++)
				rotorx_maps.insert(pair<char, char>(rotor2_encrypt[i], rotor3_encrypt[i]));
			break;
		}
		return rotorx_maps;
	}

}rotor_wiring;

typedef enum STATE{
	rotor_1 = 1,
	rotor_2 = 2,
	rotor_3 = 3,
} state;

string Encrypt(string& message, std::map<char, char> rotor_map, int& rotor, int& r1_possition, int& r2_possition, int& r3_possition)
{
switch(rotor){
case  rotor_1: {
	auto it = rotor_map.begin();
	for (int i = 0; i < message.size(); i++) {
		if (r1_possition < 28)
			r1_possition++;
		else {
			r1_possition = 0;
			r2_possition++;
		}
		it = rotor_map.find(message[i]);
		if (it != rotor_map.end()) {
			message[i] = it->second + r1_possition;
		}
		cout << it->first << " -> " << it->second << endl;
	}
	rotor = 2;
	if (rotor <= 3 && rotor > 0)
		Encrypt(message, rotor_wiring.Encrypted_letters(rotor_wiring.rotor2_maps, rotor_wiring.original, 2), rotor, rotor_wiring.r1_possition, rotor_wiring.r2_possition, rotor_wiring.r3_possition);
	break;
}
case rotor_2: {
	auto it = rotor_map.begin();
	for (int i = 0; i < message.size(); i++) {
		if (r1_possition < 28)
			r1_possition++;
		else {
			r1_possition = 0;
			r3_possition++;
		}

		it = rotor_map.find(message[i]);
		if (it != rotor_map.end()) {
			message[i] = it->second + r1_possition + r2_possition;
		}
	}
	rotor = 3;
	if (rotor <= 3 && rotor > 0)
		Encrypt(message, rotor_wiring.Encrypted_letters(rotor_wiring.rotor3_maps, rotor_wiring.original, 3), rotor, rotor_wiring.r1_possition, rotor_wiring.r2_possition, rotor_wiring.r3_possition);
	break;
}
case rotor_3: {
	auto it = rotor_map.begin();
	for (int i = 0; i < message.size(); i++) {
		if (r1_possition < 28)
			r1_possition++;
		else {
			r1_possition = 0;
			r2_possition++;
		}

		it = rotor_map.find(message[i]);;
		if (it != rotor_map.end()) {
			message[i] = it->second + r1_possition + r2_possition + r3_possition;
			cout << it->first << " -> " << it->second << endl;
		}
	}
	rotor = 4;
	break;
}
        default:
                cout << "Rotor error" << endl;
        break;
}

return message;
} 


int main(int argc, char *argv[])
{

int possition = 0;
int rotor = 1;
string message;


for (int i = 1; i < argc; ++i)
	message += argv[i];
 
map<char, char> begin_encryption = rotor_wiring.Encrypted_letters(rotor_wiring.rotor1_maps, rotor_wiring.original, 1);
cout << Encrypt(message, begin_encryption, rotor, rotor_wiring.r1_possition, rotor_wiring.r2_possition, rotor_wiring.r3_possition) << endl;

return 0;
}
